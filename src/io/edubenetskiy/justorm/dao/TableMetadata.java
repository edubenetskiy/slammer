package io.edubenetskiy.justorm.dao;

import io.edubenetskiy.justorm.annotation.ColumnField;
import io.edubenetskiy.justorm.annotation.ColumnGetter;
import io.edubenetskiy.justorm.annotation.ColumnSetter;
import io.edubenetskiy.justorm.annotation.Table;
import io.edubenetskiy.justorm.exception.MissingTableAnnotationException;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TableMetadata<T>
{
    private final String tableName;
    private final List<ColumnMetadata> columns;
    private final ColumnMetadata primaryKey;

    public TableMetadata(Class<T> clazz)
    {
        if (!clazz.isAnnotationPresent(Table.class)) {
            throw new MissingTableAnnotationException(clazz);
        }

        Table table = clazz.getAnnotation(Table.class);
        this.tableName = table.tableName().isEmpty() ?
            clazz.getSimpleName().toLowerCase() :
            table.tableName();

        Stream<ColumnMetadata> fieldColumns =
            Arrays.stream(clazz.getDeclaredFields())
                  .filter(field -> field.isAnnotationPresent(ColumnField.class))
                  .map(ColumnMetadata::new);

        Map<String, Method> getters =
            Arrays.stream(clazz.getDeclaredMethods())
                  .filter(method -> method.isAnnotationPresent(ColumnGetter.class))
                  .collect(Collectors.toMap(method -> method.getAnnotation(ColumnGetter.class).name(),
                                            method -> method));

        Map<String, Method> setters =
            Arrays.stream(clazz.getDeclaredMethods())
                  .filter(method -> method.isAnnotationPresent(ColumnSetter.class))
                  .collect(Collectors.toMap(method -> method.getAnnotation(ColumnSetter.class).name(),
                                            method -> method));

        Stream<ColumnMetadata> methodColumns =
            Stream.concat(getters.keySet().stream(),
                          setters.keySet().stream())
                  .distinct()
                  .map(columnName -> {
                      final Method getter = getters.get(columnName);
                      final Method setter = setters.get(columnName);
                      if (getter == null)
                          throw new RuntimeException(
                              String.format("no getter found for column ‘%s’", columnName));
                      if (setter == null)
                          throw new RuntimeException(
                              String.format("no setter found for column ‘%s’", columnName));
                      return new ColumnMetadata(getter, setter);
                  });

        columns = Stream.concat(fieldColumns, methodColumns).collect(Collectors.toList());

        final List<ColumnMetadata> primaryKeys =
            columns.stream()
                   .filter(ColumnMetadata::isPrimaryKey)
                   .collect(Collectors.toList());

        // TODO: Support multiple primary key columns
        if (primaryKeys.size() != 1) {
            throw new RuntimeException("the table must have exactly one primary key");
        }

        primaryKey = primaryKeys.get(0);
    }

    public String getTableName()
    {
        return tableName;
    }

    public List<ColumnMetadata> getColumns()
    {
        return columns;
    }

    public ColumnMetadata getPrimaryKey()
    {
        return primaryKey;
    }
}
