package io.edubenetskiy.justorm.dao;

import java.sql.Connection;
import java.util.HashMap;

/**
 * Class which caches created {@link DataAccessObject}s.
 * <p>
 * Since instantiation of the DAO is a bit expensive, this class is used in an
 * attempt to only create a DAO once for each class.
 * <p>
 * Sometimes internal DAOs are used to support such features as auto-refreshing
 * of foreign fields or collections of sub-objects.
 */
public class DataAccessObjectManager
{
    private Connection connection;
    private HashMap<Class, DataAccessObject> cache;

    /**
     * Creates a {@link DataAccessObjectManager} for the given database
     * connection.
     *
     * @param connection a connection for which a DAO manager will be created.
     */
    public DataAccessObjectManager(Connection connection)
    {
        this.connection = connection;
        this.cache = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public <T, PrimaryKey> DataAccessObject<T, PrimaryKey> getDao(Class<T> clazz)
    {
        return cache.computeIfAbsent(clazz, this::createDao);
    }

    private <T, PrimaryKey> DataAccessObject<T, PrimaryKey> createDao(Class<T> clazz)
    {
        return new DataAccessObjectImpl<>(connection, clazz);
    }
}
