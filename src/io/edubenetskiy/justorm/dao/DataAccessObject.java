package io.edubenetskiy.justorm.dao;

import io.edubenetskiy.justorm.annotation.ColumnField;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

/**
 * Data Access Objects handle object persistence, reading and writing data from
 * database.
 *
 * @param <T> the class containing data which should be persisted.
 * @param <K> the class of the primary key associated with the class.
 */
// TODO: Implement select select with filters
// TODO: Implement SQL select builder
public interface DataAccessObject<T, K>
{
    /**
     * Create a new row in the database from an object.
     * <p>
     * If the object being created uses {@link ColumnField#generated()},
     * then the data parameter will be modified and set with the corresponding
     * primary key from the database.
     *
     * @param item the object which contains data that has to be stored in the
     *             database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    void create(T item)
    throws SQLException;

    /**
     * Creates the table associated with the model in the database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    void migrate()
    throws SQLException;

    /**
     * Rollbacks the migration performed by {@link #migrate()} and drops the
     * table associated with the model.
     *
     * @throws SQLException in case of any SQL errors.
     */
    void rollbackMigration()
    throws SQLException;

    /**
     * Retrieves an object associated with a specific primary key.
     *
     * @param primaryKey an identifier that matches a specific row the database
     *
     * @return an <code>Optional</code> containing an object associated with the
     * given primary key, or an empty <code>Optional</code> if no matches was
     * found in the database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    Optional<T> fetchByKey(K primaryKey)
    throws SQLException;

    /**
     * Retrieves all records from the database.
     *
     * @return a <code>Collection</code> containing all records retrieved from
     * the database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    Collection<T> fetchAll()
    throws SQLException;

    /**
     * Deletes a row associated with a specific primary key.
     *
     * @param primaryKey the identifier of the item that will be deleted from
     *                   the database.
     *
     * @return the number of rows updated in the database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    boolean deleteByKey(K primaryKey)
    throws SQLException;

    /**
     * Deletes all rows in the table.
     *
     * @return the number of rows updated in the database.
     *
     * @throws SQLException in case of any SQL errors.
     */
    int deleteAll()
    throws SQLException;

    void refresh(T item)
    throws SQLException;
}
