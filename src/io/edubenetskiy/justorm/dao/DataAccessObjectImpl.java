package io.edubenetskiy.justorm.dao;

import com.sun.deploy.util.StringUtils;
import com.sun.istack.internal.logging.Logger;
import io.edubenetskiy.justorm.accessor.MemberAccessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// TODO: Store constants such as table & field names as static fields
class DataAccessObjectImpl<T, K> implements DataAccessObject<T, K>
{
    private final Connection connection;
    private final Class<T> clazz;
    private final TableMetadata<T> metadata;

    /**
     * Creates new Data Access Object for a given class.
     *
     * @param connection a database connection which should be used in the DAO
     *                   to persist object data.
     * @param clazz      a <code>Class</code> representing the data type which
     *                   should be persisted.
     */
    DataAccessObjectImpl(Connection connection, Class<T> clazz)
    {
        this.connection = connection;
        this.clazz = clazz;
        this.metadata = new TableMetadata<>(clazz);
    }

    @Override
    public void create(T item)
    throws SQLException
    {
        // Generate a query
        StringBuffer sql = new StringBuffer();
        sql.append("INSERT INTO ");
        sql.append(metadata.getTableName());
        sql.append("(");
        final List<ColumnMetadata> columns = metadata.getColumns();
        final List<ColumnMetadata> generatedColumns =
            columns.stream()
                   .filter(ColumnMetadata::isGenerated)
                   .collect(Collectors.toList());
        final List<ColumnMetadata> directColumns =
            columns.stream()
                   .filter(columnMetadata -> !columnMetadata.isGenerated())
                   .collect(Collectors.toList());

        sql.append(StringUtils.join(
            directColumns.stream()
                         .map(ColumnMetadata::getColumnName)
                         .collect(Collectors.toList()), ", "));
        sql.append(") VALUES (");
        sql.append(StringUtils.join(
            directColumns.stream()
                         .map(x -> "?")
                         .collect(Collectors.toList()), ", "));
        sql.append(")");
        sql.append(" RETURNING ");
        sql.append(StringUtils.join(
            generatedColumns.stream()
                            .map(ColumnMetadata::getColumnName)
                            .collect(Collectors.toList()), ", "));
        sql.append("");
        Logger.getLogger(DataAccessObjectImpl.class).fine(sql.toString());

        // Prepare a statement
        final PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
        for (int i = 0; i < directColumns.size(); i++) {
            ColumnMetadata field = directColumns.get(i);
            final Object value = field.getMemberAccessor().get(item);
            final Object sqlValue = field.getTypeAdapter().convertJavaToSql(value);
            Logger.getLogger(DataAccessObjectImpl.class).fine(String.format("field %d value: %s%n", i + 1, sqlValue));
            preparedStatement.setObject(i + 1, sqlValue);
        }
        preparedStatement.executeQuery();
        final int result = preparedStatement.getUpdateCount(); // TODO: Debug returning value
        final ResultSet resultSet = preparedStatement.getResultSet();

        // Update the object
        if (resultSet.next()) {
            updateObjectFromResultSet(item, resultSet, generatedColumns);
        }
    }

    @Override
    public void migrate()
    throws SQLException
    {
        StringBuilder sql = new StringBuilder()
            .append("CREATE TABLE IF NOT EXISTS ")
            .append(metadata.getTableName())
            .append(" (\n\t");
        for (ColumnMetadata field : metadata.getColumns()) {
            sql.append("\t")
               .append(field.getColumnName())
               .append(" ")
               .append(field.getSqlTypeName());
            if (field.isPrimaryKey()) sql.append(" PRIMARY KEY");
            if (field.isUnique()) sql.append(" UNIQUE");
            if (field.isNotNull()) sql.append(" NOT NULL");
            sql.append(",\n");
        }
        sql.replace(sql.length() - 2, sql.length(), "\n)");
        Logger.getLogger(DataAccessObjectImpl.class).fine(sql.toString());
        connection.prepareStatement(sql.toString()).execute();
    }

    @Override
    public void rollbackMigration()
    throws SQLException
    {
        String sql = String.format("DROP TABLE IF EXISTS %s", metadata.getTableName());
        Logger.getLogger(DataAccessObjectImpl.class).fine(sql);
        connection.prepareStatement(sql).execute();
    }

    @Override
    public Optional<T> fetchByKey(K primaryKey)
    throws SQLException
    {
        final ColumnMetadata primaryKeyColumn = metadata.getPrimaryKey();
        final List<ColumnMetadata> columns = metadata.getColumns();

        // TODO: Escape primary key value for SQL query
        String sql = String.format("SELECT %s FROM %s WHERE %s = ?",
                                   StringUtils.join(
                                       columns.stream()
                                              .map(ColumnMetadata::getColumnName)
                                              .collect(Collectors.toList()),
                                       ", "),
                                   metadata.getTableName(),
                                   primaryKeyColumn.getColumnName());
        Logger.getLogger(DataAccessObjectImpl.class).fine(sql);

        final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setObject(1, primaryKey);
        final ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            T record;
            try {
                record = clazz.newInstance();
            } catch (IllegalAccessException e) {
                throw new RuntimeException("the target class must implement the default constructor", e);
            } catch (InstantiationException e) {
                throw new RuntimeException("the target row type is not instantiable", e);
            }
            updateObjectFromResultSet(record, resultSet, columns);
            return Optional.of(record);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Collection<T> fetchAll()
    throws SQLException
    {
        final List<ColumnMetadata> columns = metadata.getColumns();

        // TODO: Escape primary key value for SQL query
        String sql = String.format("SELECT %s FROM %s",
                                   StringUtils.join(
                                       columns.stream()
                                              .map(ColumnMetadata::getColumnName)
                                              .collect(Collectors.toList()),
                                       ", "),
                                   metadata.getTableName());
        Logger.getLogger(DataAccessObjectImpl.class).fine(sql);

        ArrayList<T> results = new ArrayList<>();
        final ResultSet resultSet = connection.prepareStatement(sql).executeQuery();
        while (resultSet.next()) {
            T record;
            try {
                record = clazz.newInstance();
            } catch (IllegalAccessException e) {
                throw new RuntimeException("the target class must implement the default constructor", e);
            } catch (InstantiationException e) {
                throw new RuntimeException("the target row type is not instantiable", e);
            }
            updateObjectFromResultSet(record, resultSet, columns);
            results.add(record);
        }
        return results;
    }

    @Override
    public boolean deleteByKey(K primaryKey)
    throws SQLException
    {
        final ColumnMetadata primaryKeyField = metadata.getPrimaryKey();

        // TODO: Escape primary key value for SQL query
        String sql = String.format("DELETE FROM %s WHERE %s = %s",
                                   metadata.getTableName(),
                                   primaryKeyField.getColumnName(),
                                   primaryKey);
        final int deletedRows = connection.prepareStatement(sql).executeUpdate();
        return deletedRows > 0;
    }

    @Override
    public int deleteAll()
    throws SQLException
    {
        String sql = String.format("DELETE FROM %s",
                                   metadata.getTableName());
        return connection.prepareStatement(sql).executeUpdate();
    }

    @Override
    public void refresh(T item)
    throws SQLException
    {
        @SuppressWarnings("unchecked")
        Optional<T> original = fetchByKey((K) metadata.getPrimaryKey().getMemberAccessor().get(item));
        if (original.isPresent()) {
            for (ColumnMetadata column : metadata.getColumns()) {
                final MemberAccessor accessor = column.getMemberAccessor();
                accessor.set(item, accessor.get(original));
            }
        }
    }

    private void updateObjectFromResultSet(T object, ResultSet results, List<ColumnMetadata> columns)
    throws SQLException
    {
        for (int i = 0, j = 1, columnsSize = columns.size(); i < columnsSize; i++, j++)
        {
            ColumnMetadata column = columns.get(i);
            final Object sqlValue = results.getObject(j);
            final Object javaValue = column.getTypeAdapter().convertSqlToJava(sqlValue);
            column.getMemberAccessor().set(object, javaValue);
        }
    }
}
