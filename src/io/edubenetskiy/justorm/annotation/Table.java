package io.edubenetskiy.justorm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a table mapping for a given class.
 * <p>
 * The target class must implement a default constructor.
 */
// TODO: Automatic table name inference
// TODO: Provide API for defining relations
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table
{
    /**
     * Name of the table mapped to the class.
     */
    String tableName();
}
