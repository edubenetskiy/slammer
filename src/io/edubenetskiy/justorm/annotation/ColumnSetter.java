package io.edubenetskiy.justorm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines column mapping for a given field setter. Must be added to a setter of
 * the field being mapped.
 * <p>
 * <em>Note:</em> Once this annotation is applied, the corresponding field
 * getter must be annotated with {@link ColumnGetter}.
 */
// TODO: Automatically resolve corresponding getter and infer column name
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ColumnSetter
{
    String name();
}
