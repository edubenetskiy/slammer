package io.edubenetskiy.justorm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines column mapping for a given field. Must be added to a getter or a
 * setter of the field being mapped.
 */
// TODO: Support generated fields
// TODO: Explicit SQL types
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ColumnField
{
    String INFERRED_NAME = "";

    String name() default INFERRED_NAME;

    boolean primaryKey() default false;

    boolean generated() default false;

    boolean unique() default false;

    boolean notNull() default false;
}
