package io.edubenetskiy.justorm.exception;

public class MemberAccessException extends RuntimeException
{

    public MemberAccessException()
    {
        super();
    }

    public MemberAccessException(String message)
    {
        super(message);
    }

    public MemberAccessException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MemberAccessException(Throwable cause)
    {
        super(cause);
    }

    public MemberAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
