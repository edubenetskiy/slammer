package io.edubenetskiy.justorm.exception;

import io.edubenetskiy.justorm.annotation.Table;

public class MissingTableAnnotationException extends RuntimeException
{

    public <T> MissingTableAnnotationException(Class<T> clazz)
    {
        this(clazz, null);
    }

    public <T> MissingTableAnnotationException(Class<T> clazz, Throwable cause)
    {
        super(String.format("the class %s must be annotated with %s to be persisted",
                            clazz.getName(),
                            Table.class.getSimpleName()),
              cause);
    }
}
