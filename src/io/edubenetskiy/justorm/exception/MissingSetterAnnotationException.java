package io.edubenetskiy.justorm.exception;

import io.edubenetskiy.justorm.annotation.ColumnSetter;

import java.lang.reflect.Method;

class MissingSetterAnnotationException extends RuntimeException
{
    MissingSetterAnnotationException(Method method)
    {
        this(method, null);
    }

    private <T> MissingSetterAnnotationException(Method method, Throwable cause)
    {
        super(String.format("the method %s must be annotated with %s to be persisted",
                            method.getName(),
                            ColumnSetter.class.getSimpleName()),
              cause);
    }
}
