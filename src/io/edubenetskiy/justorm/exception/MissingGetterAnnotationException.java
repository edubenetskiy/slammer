package io.edubenetskiy.justorm.exception;

import io.edubenetskiy.justorm.annotation.ColumnGetter;

import java.lang.reflect.Method;

public class MissingGetterAnnotationException extends RuntimeException
{
    public MissingGetterAnnotationException(Method method)
    {
        this(method, null);
    }

    public <T> MissingGetterAnnotationException(Method method, Throwable cause)
    {
        super(String.format("the method %s must be annotated with %s to be persisted",
                            method.getName(),
                            ColumnGetter.class.getSimpleName()),
              cause);
    }
}
