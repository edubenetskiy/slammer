package io.edubenetskiy.justorm.exception;

import io.edubenetskiy.justorm.annotation.Table;

import java.lang.reflect.Field;

public class MissingFieldAnnotationException extends RuntimeException
{

    public MissingFieldAnnotationException(Field field)
    {
        this(field, null);
    }

    public <T> MissingFieldAnnotationException(Field field, Throwable cause)
    {
        super(String.format("the column %s must be annotated with %s to be persisted",
                            field.getName(),
                            Table.class.getSimpleName()),
              cause);
    }
}
