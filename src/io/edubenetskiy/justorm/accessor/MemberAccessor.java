package io.edubenetskiy.justorm.accessor;

import io.edubenetskiy.justorm.exception.MemberAccessException;

public interface MemberAccessor
{
    /**
     * @throws MemberAccessException
     */
    Object get(Object target);

    /**
     * @throws MemberAccessException
     */
    void set(Object target, Object value);
}

