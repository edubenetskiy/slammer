package io.edubenetskiy.justorm.accessor;

import io.edubenetskiy.justorm.exception.MemberAccessException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodAccessor implements MemberAccessor
{
    private final Method getter;
    private final Method setter;

    public MethodAccessor(Method getter, Method setter)
    {
        // TODO: Ensure setter accepts exactly 1 parameter
        if (setter.getParameterCount() != 1) {
            throw new RuntimeException("setter must accept exactly 1 parameter");
        }
        // TODO: Ensure getter's return type is compatible to setter's param type
        Class<?> getterType = getter.getReturnType();
        Class<?> setterType = setter.getParameterTypes()[0];
        if (false) { // TODO
            throw new RuntimeException("getter and setter types should be compatible");
        }
        // TODO: Ensure getter and setter are both accessible
        getter.setAccessible(true);
        setter.setAccessible(true);

        this.getter = getter;
        this.setter = setter;
    }

    @Override
    public Object get(Object target)
    throws MemberAccessException
    {
        try {
            return getter.invoke(target);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new MemberAccessException(e);
        }
    }

    @Override
    public void set(Object target, Object value)
    {
        try {
            setter.invoke(target, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new MemberAccessException(e);
        }
    }
}
