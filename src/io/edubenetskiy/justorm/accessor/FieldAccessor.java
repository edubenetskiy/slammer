package io.edubenetskiy.justorm.accessor;

import io.edubenetskiy.justorm.exception.MemberAccessException;

import java.lang.reflect.Field;

public class FieldAccessor implements MemberAccessor
{
    private Field field;

    public FieldAccessor(Field field)
    {
        this.field = field;
        // TODO: Ensure field is accessible
        field.setAccessible(true);
    }

    @Override
    public Object get(Object target)
    {
        try {
            return field.get(target);
        } catch (IllegalAccessException e) {
            throw new MemberAccessException(e);
        }
    }

    @Override
    public void set(Object target, Object value)
    {
        try {
            field.set(target, value);
        } catch (IllegalAccessException e) {
            throw new MemberAccessException(e);
        }
    }
}
