/**
 * This package provides the functionality for a Object Relational Mapping
 * between Java classes and SQL databases.
 */
package io.edubenetskiy.justorm;
