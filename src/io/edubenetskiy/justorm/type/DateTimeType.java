package io.edubenetskiy.justorm.type;

import java.util.Date;
import java.util.Optional;

public class DateTimeType extends TypeAdapter
{
    private static final String TYPE_NAME = "TIMESTAMP";

    public DateTimeType() {}

    public static Optional<DateTimeType> forSqlType(String sqlType)
    {
        if (TYPE_NAME.equalsIgnoreCase(sqlType)) {
            return Optional.of(new DateTimeType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<DateTimeType> forJavaType(Class<?> clazz)
    {
        if (Date.class.isAssignableFrom(clazz)) {
            return Optional.of(new DateTimeType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
