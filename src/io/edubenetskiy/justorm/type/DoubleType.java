package io.edubenetskiy.justorm.type;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DoubleType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN = Pattern.compile("(?i)^(FLOAT|DOUBLE PRECISION)$");
    private static final String TYPE_NAME = "DOUBLE PRECISION";

    public DoubleType() {}

    public static Optional<DoubleType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            return Optional.of(new DoubleType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<DoubleType> forJavaType(Class<?> clazz)
    {
        if (Double.class.isAssignableFrom(clazz) ||
            Double.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new DoubleType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
