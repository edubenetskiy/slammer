package io.edubenetskiy.justorm.type;

import java.util.Optional;

public interface JavaTypeProvider
{
    Optional<? extends TypeAdapter> provide(Class<?> javaType);
}
