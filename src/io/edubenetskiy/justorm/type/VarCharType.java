package io.edubenetskiy.justorm.type;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VarCharType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN = Pattern.compile("(?i)^VARCHAR\\((\\d+)\\)$");
    private static final String TYPE_NAME_FORMAT = "VARCHAR(%d)";

    private static final long DEFAULT_WIDTH = 64;

    private final long width;
    private final String typeName;

    public VarCharType()
    {
        this(DEFAULT_WIDTH);
    }

    public VarCharType(long width)
    {
        // TODO: Assert width > 0
        this.width = width;
        this.typeName = String.format(TYPE_NAME_FORMAT, width);
    }

    public static Optional<VarCharType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            final long width = Long.parseLong(matcher.group(1));
            return Optional.of(new VarCharType(width));
        } else {
            return Optional.empty();
        }
    }

    public static Optional<VarCharType> forJavaType(Class<?> clazz)
    {
        if (String.class.isAssignableFrom(clazz)) {
            return Optional.of(new VarCharType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return typeName;
    }

    public long getWidth()
    {
        return width;
    }
}
