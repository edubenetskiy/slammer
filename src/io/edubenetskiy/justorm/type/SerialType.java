package io.edubenetskiy.justorm.type;

import java.util.Optional;

public class SerialType extends TypeAdapter
{
    private static final String TYPE_NAME = "SERIAL";

    public SerialType() {}

    public static Optional<SerialType> forSqlType(String sqlType)
    {
        if (TYPE_NAME.equalsIgnoreCase(sqlType)) {
            return Optional.of(new SerialType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<SerialType> forJavaType(Class<?> clazz)
    {
        if (Integer.class.isAssignableFrom(clazz) ||
            Integer.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new SerialType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
