package io.edubenetskiy.justorm.type;

import java.util.Optional;

public class BooleanType extends TypeAdapter
{
    private static final String TYPE_NAME = "BOOLEAN";

    public BooleanType() {}

    public static Optional<BooleanType> forSqlType(String sqlType)
    {
        if (TYPE_NAME.equalsIgnoreCase(sqlType)) {
            return Optional.of(new BooleanType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<BooleanType> forJavaType(Class<?> clazz)
    {
        if (Boolean.class.isAssignableFrom(clazz) ||
            Boolean.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new BooleanType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
