package io.edubenetskiy.justorm.type;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZonedDateTimeType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN =
        Pattern.compile("(?i)^TIMESTAMP(TZ| WITH TIME ZONE)$");
    private static final String TYPE_NAME = "TIMESTAMP WITH TIME ZONE";

    public ZonedDateTimeType() {}

    public static Optional<ZonedDateTimeType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            return Optional.of(new ZonedDateTimeType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<ZonedDateTimeType> forJavaType(Class<?> clazz)
    {
        if (ZonedDateTime.class.isAssignableFrom(clazz)) {
            return Optional.of(new ZonedDateTimeType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }

    @Override
    public Object convertSqlToJava(Object object)
    {
        // TODO: Store timezone properly
        return ((Timestamp) object).toLocalDateTime()
                                   .atZone(ZoneId.systemDefault());
    }

    @Override
    public Object convertJavaToSql(Object object)
    {
        return ((ZonedDateTime) object).toOffsetDateTime();
    }
}
