package io.edubenetskiy.justorm.type;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShortType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN = Pattern.compile("(?i)^(SMALLINT|INT2)$");
    private static final String TYPE_NAME = "SMALLINT";

    public ShortType() {}

    public static Optional<ShortType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            return Optional.of(new ShortType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<ShortType> forJavaType(Class<?> clazz)
    {
        if (Short.class.isAssignableFrom(clazz) ||
            Short.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new ShortType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
