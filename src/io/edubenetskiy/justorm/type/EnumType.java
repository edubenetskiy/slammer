package io.edubenetskiy.justorm.type;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public class EnumType extends TypeAdapter
{
    private static final String TYPE_NAME_FORMAT = "VARCHAR(%d)";
    private final Class<? extends Enum> clazz;
    private final int maxWidth;
    private Method valueOf;

    public EnumType(Class<? extends Enum> clazz)
    {
        this.clazz = clazz;
        try {
            this.valueOf = clazz.getMethod("valueOf", String.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("the enum type is expected to implement the ‘valueOf(String)’ method", e);
        }
        this.maxWidth = Arrays.stream(clazz.getEnumConstants())
                              .map(Object::toString)
                              .map(String::length)
                              .max(Comparator.naturalOrder())
                              .orElse(0);
    }

    public static Optional<EnumType> forSqlType(String sqlType)
    {
        // TODO
        return Optional.empty();
    }

    public static Optional<EnumType> forJavaType(Class<?> clazz)
    {
        if (clazz.isEnum()) {
            @SuppressWarnings("unchecked") final Class<? extends Enum> enumClass = (Class<? extends Enum>) clazz;
            return Optional.of(new EnumType(enumClass));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return String.format(TYPE_NAME_FORMAT, maxWidth);
    }

    @Override
    public Object convertSqlToJava(Object object)
    {
        try {
            return valueOf.invoke(null, object);
        } catch (IllegalAccessException | InvocationTargetException e) {
            // TODO
        }
        return null;
    }

    @Override
    public Object convertJavaToSql(Object object)
    {
        return object.toString();
    }
}
