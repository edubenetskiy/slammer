package io.edubenetskiy.justorm.type;

import java.util.Optional;

@FunctionalInterface
public interface SqlTypeProvider
{
    Optional<? extends TypeAdapter> provide(String sqlType);
}
