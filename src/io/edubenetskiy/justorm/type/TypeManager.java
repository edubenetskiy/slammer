package io.edubenetskiy.justorm.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TypeManager
{
    private static final List<SqlTypeProvider> registeredSqlProviders = new ArrayList<>();
    private static final List<JavaTypeProvider> registeredJavaProviders = new ArrayList<>();

    private static TypeManager INSTANCE;

    private TypeManager()
    {
        registerSqlTypeProvider(BooleanType::forSqlType);
        registerSqlTypeProvider(VarCharType::forSqlType);
        registerSqlTypeProvider(SerialType::forSqlType);
        registerSqlTypeProvider(DoubleType::forSqlType);
        registerSqlTypeProvider(LongType::forSqlType);
        registerSqlTypeProvider(IntegerType::forSqlType);
        registerSqlTypeProvider(ShortType::forSqlType);
        registerSqlTypeProvider(ZonedDateTimeType::forSqlType);
        registerSqlTypeProvider(DateTimeType::forSqlType);
        registerSqlTypeProvider(EnumType::forSqlType);

        registerJavaTypeProvider(BooleanType::forJavaType);
        registerJavaTypeProvider(VarCharType::forJavaType);
        registerJavaTypeProvider(SerialType::forJavaType);
        registerJavaTypeProvider(DoubleType::forJavaType);
        registerJavaTypeProvider(LongType::forJavaType);
        registerJavaTypeProvider(IntegerType::forJavaType);
        registerJavaTypeProvider(ShortType::forJavaType);
        registerJavaTypeProvider(ZonedDateTimeType::forJavaType);
        registerJavaTypeProvider(DateTimeType::forJavaType);
        registerJavaTypeProvider(EnumType::forJavaType);
    }

    public static TypeManager getManager()
    {
        if (INSTANCE == null) INSTANCE = new TypeManager();
        return INSTANCE;
    }

    public void registerSqlTypeProvider(SqlTypeProvider sqlTypeProvider)
    {
        registeredSqlProviders.add(sqlTypeProvider);
    }

    public void registerJavaTypeProvider(JavaTypeProvider javaTypeProvider)
    {
        registeredJavaProviders.add(javaTypeProvider);
    }

    public Optional<? extends TypeAdapter> findAdapterForSqlType(String sqlType)
    {
        return registeredSqlProviders.stream()
                                     .map(provider -> provider.provide(sqlType))
                                     .filter(Optional::isPresent)
                                     .map(Optional::get)
                                     .reduce((a, b) -> b);
    }

    public Optional<? extends TypeAdapter> findAdapterForJavaType(Class<?> sqlType)
    {
        return registeredJavaProviders.stream()
                                      .map(provider -> provider.provide(sqlType))
                                      .filter(Optional::isPresent)
                                      .map(Optional::get)
                                      .reduce((a, b) -> b);
    }
}
