package io.edubenetskiy.justorm.type;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LongType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN = Pattern.compile("(?i)^(BIGINT|INT8)$");
    private static final String TYPE_NAME = "BIGINT";

    public LongType() {}

    public static Optional<LongType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            return Optional.of(new LongType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<LongType> forJavaType(Class<?> clazz)
    {
        if (Long.class.isAssignableFrom(clazz) ||
            Long.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new LongType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
