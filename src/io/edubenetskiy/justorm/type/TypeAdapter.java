package io.edubenetskiy.justorm.type;

// TODO: Document properly
public abstract class TypeAdapter
{
    /**
     * Returns a fully qualified SQL name for the represented type.
     */
    abstract public String getSqlTypeName();

    public Object convertSqlToJava(Object object)
    {
        return object;
    }

    public Object convertJavaToSql(Object object)
    {
        return object;
    }
}
