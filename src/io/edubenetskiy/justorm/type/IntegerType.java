package io.edubenetskiy.justorm.type;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntegerType extends TypeAdapter
{
    private static final Pattern TYPE_NAME_PATTERN = Pattern.compile("(?i)^(INTEGER|INT4)$");
    private static final String TYPE_NAME = "INTEGER";

    public IntegerType() {}

    public static Optional<IntegerType> forSqlType(String sqlType)
    {
        final Matcher matcher = TYPE_NAME_PATTERN.matcher(sqlType);
        if (matcher.matches()) {
            return Optional.of(new IntegerType());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<IntegerType> forJavaType(Class<?> clazz)
    {
        if (Integer.class.isAssignableFrom(clazz) ||
            Integer.TYPE.isAssignableFrom(clazz))
        {
            return Optional.of(new IntegerType());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public String getSqlTypeName()
    {
        return TYPE_NAME;
    }
}
