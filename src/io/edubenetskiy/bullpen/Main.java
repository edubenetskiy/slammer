package io.edubenetskiy.bullpen;

import io.edubenetskiy.justorm.dao.DataAccessObject;
import io.edubenetskiy.justorm.dao.DataAccessObjectManager;
import org.postgresql.ds.PGSimpleDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.logging.LogManager;

public class Main
{
    public static void main(String[] args)
    throws SQLException
    {
        try {
            LogManager.getLogManager().readConfiguration(
                Main.class.getResourceAsStream("/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }

        final String url = "jdbc:postgresql://localhost:5432/edubenetskiy";
        System.out.println(url);

        final PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setUrl(url);
        Connection connection = dataSource.getConnection("io/edubenetskiy", null);
        DataAccessObject<Person, Integer> dao = new DataAccessObjectManager(connection).getDao(Person.class);
        dao.rollbackMigration();
        dao.migrate();

        final Person person = new Person("Егор Дубенецкий", ZonedDateTime.now());
        dao.create(person);

        dao.create(new Person(2, "Николай Васильев", ZonedDateTime.now().minusDays(5).minusHours(2).minusMinutes(30).minusSeconds(13)));
        person.delete(dao);

        System.out.println(dao.fetchByKey(2).isPresent());

        Person fetched = dao.fetchByKey(2).get();
        System.out.println(fetched.getTime());
    }
}
