package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;

public class ClearResponse extends Response
{
    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.clear();
    }
}
