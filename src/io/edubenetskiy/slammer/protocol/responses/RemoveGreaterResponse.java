package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;

public class RemoveGreaterResponse extends Response
{
    private final int id;

    public RemoveGreaterResponse(int id)
    {
        this.id = id;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.removeGreater(id);
    }
}
