package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;

public class DeleteResponse extends Response
{
    private final int id;

    public DeleteResponse(int id)
    {
        this.id = id;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.delete(id);
    }
}
