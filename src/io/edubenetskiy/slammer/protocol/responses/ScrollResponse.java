package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;
import io.edubenetskiy.slammer.client.Record;

import java.util.List;

public class ScrollResponse extends Response
{
    private final List<Record> scroll;

    public ScrollResponse(List<Record> scroll)
    {
        this.scroll = scroll;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.scroll(scroll);
    }
}

