package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;
import io.edubenetskiy.slammer.model.Shorty;

public class UpdateResponse extends Response
{
    private final Integer id;
    private final Shorty shorty;

    public UpdateResponse(Integer id, Shorty shorty)
    {
        this.id = id;
        this.shorty = shorty;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.update(id, shorty);
    }
}
