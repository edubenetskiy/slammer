package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;
import io.edubenetskiy.slammer.model.Shorty;

public class InsertResponse extends Response
{
    private final int id;
    private final Shorty shorty;

    public InsertResponse(int id, Shorty shorty)
    {
        this.id = id;
        this.shorty = shorty;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        clientCopy.insert(id, shorty);
    }
}
