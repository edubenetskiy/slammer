package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;
import io.edubenetskiy.slammer.protocol.requests.Request;
import javafx.application.Platform;
import javafx.scene.control.Alert;

public class FailureResponse extends Response
{
    private final Request request;

    public FailureResponse(Request request)
    {
        this.request = request;
    }

    @Override
    public void execute(ClientCopy clientCopy)
    {
        // TODO: Make FailureResponse alerts more human-readable
        Platform.runLater(() -> {
            final Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(request.toString());
            alert.setHeaderText("Could not execute a query, sorry.");
            alert.show();
        });
    }
}
