package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;

import java.io.Serializable;

public abstract class Response implements Serializable
{
    abstract public void execute(ClientCopy clientCopy);
}
