package io.edubenetskiy.slammer.protocol.responses;

import io.edubenetskiy.slammer.client.ClientCopy;

public class NoopResponse extends Response
{
    @Override
    public void execute(ClientCopy clientCopy)
    {
        // do nothing, this is a no-op
    }
}
