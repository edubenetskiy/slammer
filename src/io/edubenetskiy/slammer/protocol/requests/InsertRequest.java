package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.model.Shorty;
import io.edubenetskiy.slammer.protocol.responses.FailureResponse;
import io.edubenetskiy.slammer.protocol.responses.InsertResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

public class InsertRequest extends Request
{
    private final Shorty shorty;

    public InsertRequest(Shorty shorty)
    {
        this.shorty = shorty;
    }

    @Override
    public Response execute(Database database)
    {
        return database.create(shorty)
                       .map(id -> (Response) new InsertResponse(id, shorty))
                       .orElse(new FailureResponse(this));
    }
}
