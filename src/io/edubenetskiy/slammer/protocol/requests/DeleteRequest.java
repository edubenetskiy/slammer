package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.DeleteResponse;
import io.edubenetskiy.slammer.protocol.responses.FailureResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

public class DeleteRequest extends Request
{
    private final int id;

    public DeleteRequest(int id)
    {
        this.id = id;
    }

    @Override
    public Response execute(Database database)
    {
        return database.delete(id)
            ? new DeleteResponse(id)
            : new FailureResponse(this);
        // TODO: Notify of not found record
    }
}
