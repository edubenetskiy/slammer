package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

import java.io.Serializable;

public abstract class Request implements Serializable
{
    public abstract Response execute(Database database);
}
