package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.NoopResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

public class NoopRequest extends Request
{
    @Override
    public Response execute(Database database)
    {
        return new NoopResponse();
    }
}
