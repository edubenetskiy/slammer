package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.RemoveGreaterResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

public class RemoveGreaterRequest extends Request
{
    private final int id;

    public RemoveGreaterRequest(int id)
    {
        this.id = id;
    }

    @Override
    public Response execute(Database database)
    {
        database.removeGreater(id);
        return new RemoveGreaterResponse(id);
    }
}
