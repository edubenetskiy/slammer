package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.protocol.responses.ScrollResponse;
import io.edubenetskiy.slammer.server.Database;

public class ScrollRequest extends Request
{
    @Override
    public Response execute(Database database)
    {
        return new ScrollResponse(database.getAllRecords());
    }
}
