package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.model.Shorty;
import io.edubenetskiy.slammer.protocol.responses.FailureResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.protocol.responses.UpdateResponse;
import io.edubenetskiy.slammer.server.Database;

public class UpdateRequest extends Request
{
    private final Integer id;
    private final Shorty shorty;

    public UpdateRequest(Integer id, Shorty shorty)
    {
        this.id = id;
        this.shorty = shorty;
    }

    @Override
    public Response execute(Database database)
    {
        if (id == null) return new FailureResponse(this);
        database.update(id, shorty);
        return new UpdateResponse(id, shorty);
    }
}
