package io.edubenetskiy.slammer.protocol.requests;

import io.edubenetskiy.slammer.protocol.responses.ClearResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;
import io.edubenetskiy.slammer.server.Database;

public class ClearRequest extends Request
{
    @Override
    public Response execute(Database database)
    {
        database.deleteAll();
        return new ClearResponse();
    }
}
