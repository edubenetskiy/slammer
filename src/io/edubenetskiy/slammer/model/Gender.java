package io.edubenetskiy.slammer.model;

import io.edubenetskiy.slammer.client.I18n;
import javafx.beans.binding.StringBinding;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.Optional;

public enum Gender
{
    MALE(I18n.$("model.gender.male"), Color.DEEPSKYBLUE),
    FEMALE(I18n.$("model.gender.female"), Color.DEEPPINK);

    private final StringBinding stringRepresentation;
    private final Color associatedColor;

    Gender(StringBinding stringRepresentation, Color associatedColor)
    {
        this.stringRepresentation = stringRepresentation;
        this.associatedColor = associatedColor;
    }

    public static Optional<Gender> fromStringRepresentation(String string)
    {
        return Arrays.stream(Gender.values())
                     .filter(g -> g.getStringRepresentation()
                                   .get()
                                   .equals(string))
                     .findFirst();
    }

    public StringBinding getStringRepresentation()
    {
        return stringRepresentation;
    }

    public Color getAssociatedColor()
    {
        return associatedColor;
    }
}
