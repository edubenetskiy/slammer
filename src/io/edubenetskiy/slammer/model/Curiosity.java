package io.edubenetskiy.slammer.model;

import io.edubenetskiy.slammer.client.I18n;
import javafx.beans.binding.StringBinding;
import javafx.scene.paint.Color;

/**
 * Level of curiosity and inquisitiveness.
 */
public enum Curiosity
{
    /**
     * The shorty is absolutely indifferent.
     */
    INDIFFERENT(I18n.$("model.curiosity.indifferent"), Color.GREEN),

    /**
     * The shorty is as curious as most people.
     */
    TOLERABLE(I18n.$("model.curiosity.tolerable"), Color.BLACK),

    /**
     * The shorty tends to pry into everything.
     */
    PRYING(I18n.$("model.curiosity.prying"), Color.ORANGE),

    /**
     * The shorty annoys everybody because of his superfluous and
     * excessive curiosity.
     */
    INTRUSIVE(I18n.$("model.curiosity.intrusive"), Color.CRIMSON);

    private final StringBinding stringRepresentation;
    private final Color associatedColor;

    Curiosity(StringBinding stringRepresentation, Color associatedColor)
    {
        this.stringRepresentation = stringRepresentation;
        this.associatedColor = associatedColor;
    }

    public StringBinding getStringRepresentation()
    {
        return stringRepresentation;
    }

    public Color getAssociatedColor()
    {
        return associatedColor;
    }
}
