package io.edubenetskiy.slammer.client;

import io.edubenetskiy.slammer.model.Shorty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Objects;
import java.util.Optional;

public class Record implements Externalizable
{
    private final Shorty shorty;
    private final BooleanProperty newRecord;
    private final BooleanProperty destroyed;
    private Integer id;
    private BooleanProperty changed;

    public Record(int id, Shorty shorty)
    {
        this();
        this.id = id;
        this.newRecord.set(false);
        update(shorty);
        this.changed.set(false);
    }

    public Record()
    {
        this.id = null;
        this.newRecord = new SimpleBooleanProperty(this, "newRecord", true);

        this.shorty = new Shorty();
        this.shorty.nameProperty().addListener(observable -> changed.set(true));
        this.shorty.genderProperty().addListener(observable -> changed.set(true));
        this.shorty.dateOfImprisonmentProperty().addListener(observable -> changed.set(true));
        this.shorty.curiosityProperty().addListener(observable -> changed.set(true));
        this.shorty.innocenceProperty().addListener(observable -> changed.set(true));
        this.shorty.registeredProperty().addListener(observable -> changed.set(true));

        this.changed = new SimpleBooleanProperty(this, "changed", false);
        this.destroyed = new SimpleBooleanProperty(this, "destroyed", false);
    }

    public void update(Shorty shorty)
    {
        this.shorty.setName(shorty.getName());
        this.shorty.setGender(shorty.getGender());
        this.shorty.setDateOfImprisonment(shorty.getDateOfImprisonment());
        this.shorty.setCuriosity(shorty.getCuriosity());
        this.shorty.setInnocence(shorty.getInnocence());
        this.shorty.setRegistered(shorty.isRegistered());
    }

    public boolean delete(DatabaseConnection dao)
    {
        dao.delete(this);
        // TODO: Update destroyedProperty
        return isPersisted() && isDestroyed();
    }

    private boolean isPersisted()
    {
        return !isNewRecord() && !isDestroyed();
    }

    public boolean isDestroyed()
    {
        return destroyed.get();
    }

    public boolean isNewRecord()
    {
        return newRecord.get();
    }

    public boolean saveIfMin(DatabaseConnection dao)
    {
        if (dao.isMin(this)) {
            save(dao);
            return true;
        } else return false;
    }

    public void save(DatabaseConnection dao)
    {
        insertOrUpdate(dao);
    }

    private void insertOrUpdate(DatabaseConnection dao)
    {
        if (isNewRecord()) {
            this.newRecord.set(false);
            dao.insert(this.getShorty());
        } else {
            // XXX: It does not update the ID, which fails on save after create
            dao.update(getId(), getShorty());
        }
        this.changed.set(false);
    }

    public Shorty getShorty()
    {
        return shorty;
    }

    public Integer getId()
    {
        return id;
    }

    public void reset(DatabaseConnection dao)
    {
        if (isPersisted()) {
            Optional<Record> original = dao.findById(getId());
            original.ifPresent(record -> {
                update(record.getShorty());
                this.changed.set(false);
            });
        }
    }

    public BooleanProperty newRecordProperty()
    {
        return newRecord;
    }

    public boolean isChanged()
    {
        return changed.get();
    }

    public BooleanProperty changedProperty()
    {
        return changed;
    }

    public BooleanProperty destroyedProperty()
    {
        return destroyed;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object other)
    {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Record record = (Record) other;
        return this.isPersisted() &&
               record.isPersisted() &&
               Objects.equals(id, record.id);
    }

    @Override
    public void writeExternal(ObjectOutput out)
    throws IOException
    {
        out.writeInt(getId());
        out.writeObject(getShorty());
    }

    @Override
    public void readExternal(ObjectInput in)
    throws IOException, ClassNotFoundException
    {
        id = in.readInt();
        update((Shorty) in.readObject());
        newRecord.set(false);
    }
}
