package io.edubenetskiy.slammer.client;

import com.sun.javafx.collections.ImmutableObservableList;
import io.edubenetskiy.slammer.model.Gender;
import io.edubenetskiy.slammer.model.Shorty;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;

import static io.edubenetskiy.slammer.client.I18n.$;

public class EditForm extends GridPane
{
    private final TextField nameField;
    private final ComboBox<Gender> genderComboBox;
    private final Slider innocenceSlider;
    private final CuriosityToggleGroup curiosityToggleGroup;
    private final ToggleButton registrationToggleButton;

    private final SimpleObjectProperty<Record> editedRecord;
    private final Label changedLabel;
    private final Label stateLabel;

    private final BooleanProperty resettable;
    private final BooleanProperty savable;
    private final SimpleObjectProperty<DatabaseConnection> databaseConnection;

    public EditForm()
    {
        super();
        savable = new SimpleBooleanProperty(this, "savable", false);
        resettable = new SimpleBooleanProperty(this, "resettable", false);

        editedRecord = new SimpleObjectProperty<>(this, "editedRecord", null);
        databaseConnection = new SimpleObjectProperty<>(this, "databaseConnection", null);

        changedLabel = new Label();
        changedLabel.setFont(Font.font(16));
        changedLabel.setTextAlignment(TextAlignment.RIGHT);

        stateLabel = new Label();
        stateLabel.setFont(Font.font(16));

        nameField = new TextField();
        nameField.setMaxWidth(Double.MAX_VALUE);

        genderComboBox = new ComboBox<>(new ImmutableObservableList<>(Gender.values()));
        genderComboBox.setMaxWidth(Double.MAX_VALUE);
        genderComboBox.setCellFactory(param -> new ListCell<Gender>()
        {
            @Override
            protected void updateItem(Gender item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    textProperty().unbind();
                    setText(null);
                } else {
                    textProperty().bind(item.getStringRepresentation());
                    setTextFill(item.getAssociatedColor());
                }
            }
        });
        I18n.localeProperty()
            .addListener((observable, oldValue, newValue) -> {
                final Gender value = genderComboBox.getValue();
                genderComboBox.setValue(null);
                genderComboBox.setValue(value);
            });
        genderComboBox.setConverter(new StringConverter<Gender>()
        {
            @Override
            public String toString(Gender object)
            {
                return object.getStringRepresentation().get();
            }

            @Override
            public Gender fromString(String string)
            {
                return Gender.fromStringRepresentation(string).orElse(null);
            }
        });

        Label innocenceLabel = new Label();
        innocenceSlider = new Slider(0, 100, 50);
        innocenceLabel.setMinWidth(42);
        innocenceLabel.setAlignment(Pos.CENTER_RIGHT);
        innocenceLabel.textProperty()
                      .bind(innocenceSlider.valueProperty().asString("%.1f"));
        innocenceSlider.adjustValue(0);
        innocenceSlider.setShowTickMarks(true);
        innocenceSlider.setMaxWidth(Double.MAX_VALUE);

        final HBox innocenceBox = new HBox(innocenceSlider, innocenceLabel);
        innocenceBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(innocenceSlider, Priority.ALWAYS);

        registrationToggleButton = new ToggleButton("✓");

        curiosityToggleGroup = new CuriosityToggleGroup();

        final Button btnReset = new Button();
        btnReset.textProperty().bind($("mainWindow.menu.edit.reset"));
        btnReset.setMaxWidth(Double.MAX_VALUE);
        btnReset.setPrefHeight(50);
        btnReset.setOnAction(event -> reset());
        btnReset.setTooltip(new Tooltip("Load values from the database"));
        btnReset.disableProperty().bind(resettableProperty().not());

        final Button btnSave = new Button();
        btnSave.textProperty().bind($("mainWindow.menu.edit.save"));
        btnSave.setFont(Font.font(null, FontWeight.BOLD, -1));
        btnSave.setMaxWidth(Double.MAX_VALUE);
        btnSave.setPrefHeight(50);
        btnSave.setOnAction(event -> save());
        btnSave.setTooltip(I18n.createTooltip($("mainWindow.menu.edit.save.tooltip")));
        btnSave.setDefaultButton(true);
        btnSave.disableProperty().bind(savableProperty().not());

        final Button btnSaveIfMin = new Button();
        btnSaveIfMin.textProperty().bind($("mainWindow.menu.edit.saveIfMin"));
        btnSaveIfMin.setMaxWidth(Double.MAX_VALUE);
        btnSaveIfMin.setPrefHeight(50);
        btnSaveIfMin.setOnAction(event -> saveIfMin());

        add(stateLabel, 0, 0, 2, 1);

        add(I18n.createLabel($("model.name")), 0, 1);
        add(nameField, 1, 1);

        add(I18n.createLabel($("model.gender")), 0, 2);
        add(genderComboBox, 1, 2);

        add(I18n.createLabel($("model.innocence")), 0, 3);
        add(innocenceBox, 1, 3);

        add(I18n.createLabel($("model.registration")), 0, 4);
        add(registrationToggleButton, 1, 4);

        add(I18n.createLabel($("model.curiosity")), 0, 5);
        add(curiosityToggleGroup, 1, 5);

        add(btnReset, 0, 6);
        add(btnSave, 1, 6);

        add(btnSaveIfMin, 1, 7);

        setDatabaseConnection(null);
        setEditedRecord(null);

        savable.bind(databaseConnection.isNotNull().and(editedRecord.isNotNull()));
        resettable.bind(databaseConnection.isNotNull().and(editedRecord.isNotNull()));

        disableProperty().set(true);
        disableProperty().bind(editedRecord.isNull());

        final ColumnConstraints firstColumnConstraints = new ColumnConstraints(Region.USE_COMPUTED_SIZE);
        final ColumnConstraints secondColumnConstraints = new ColumnConstraints();
        secondColumnConstraints.setHgrow(Priority.ALWAYS);
        getColumnConstraints().add(firstColumnConstraints);
        getColumnConstraints().add(secondColumnConstraints);

        setPadding(new Insets(8));
        setHgap(8);
        setVgap(8);

        setMinWidth(256);
        setMaxWidth(512);
    }

    void reset()
    {
        if (isResettable()) {
            final Shorty shorty = editedRecord.get().getShorty();
            nameField.setText(shorty.getName());
            genderComboBox.setValue(shorty.getGender());
            curiosityToggleGroup.setValue(shorty.getCuriosity());
            innocenceSlider.setValue(shorty.getInnocence());
            registrationToggleButton.setSelected(shorty.isRegistered());
        }
    }

    public ReadOnlyBooleanProperty resettableProperty()
    {
        return resettable;
    }

    void save()
    {
        if (isSavable()) {
            final Record record = editedRecord.get();
            final Shorty shorty = record.getShorty();
            shorty.setName(nameField.getText());
            shorty.setGender(genderComboBox.getValue());
            shorty.setCuriosity(curiosityToggleGroup.getValue());
            shorty.setInnocence(innocenceSlider.getValue());
            shorty.setRegistered(registrationToggleButton.isSelected());
            record.save(databaseConnection.get());
        }
    }

    public ReadOnlyBooleanProperty savableProperty()
    {
        return savable;
    }

    void saveIfMin()
    {
        if (isSavable()) {
            final Record record = editedRecord.get();
            final Shorty shorty = record.getShorty();
            shorty.setName(nameField.getText());
            shorty.setGender(genderComboBox.getValue());
            shorty.setCuriosity(curiosityToggleGroup.getValue());
            shorty.setInnocence(innocenceSlider.getValue());
            shorty.setRegistered(registrationToggleButton.isSelected());
            if (!record.saveIfMin(databaseConnection.get())) {
                record.reset(databaseConnection.get());
                // TODO: Use I18n
                final Alert alert = new Alert(Alert.AlertType.ERROR, null);
                alert.contentTextProperty().bind($("mainWindow.menu.edit.saveIfMin.alert"));
                alert.showAndWait();
            }
        }
    }

    public void setDatabaseConnection(DatabaseConnection databaseConnection)
    {
        this.databaseConnection.set(databaseConnection);
    }

    public boolean isResettable()
    {
        return resettable.get();
    }

    public boolean isSavable()
    {
        return savable.get();
    }

    public Record getEditedRecord()
    {
        return editedRecord.get();
    }

    // TODO: Usage of class Bindings is obsolete
    public void setEditedRecord(Record editedRecord)
    {
        this.editedRecord.set(editedRecord);
        if (editedRecord == null) {
            changedLabel.textProperty().unbind();
            changedLabel.setText(null);
            stateLabel.textProperty().unbind();
            stateLabel.textProperty().bind($("editForm.title.noSelection"));
        } else {
            changedLabel.textProperty().bind(
                Bindings
                    .when(editedRecord.changedProperty())
                    .then("*")
                    .otherwise(""));
            stateLabel.textProperty().bind(
                Bindings
                    .when(editedRecord.newRecordProperty())
                    .then($("editForm.title.newRecord"))
                    .otherwise($("editForm.title.editRecord")));
        }
        reset();
    }
}
