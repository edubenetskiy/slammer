package io.edubenetskiy.slammer.client;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.ResourceBundle;

public class I18n
{
    public static final HashSet<Locale> SUPPORTED_LOCALES = new HashSet<>(Arrays.asList(
        new Locale("en"),
        new Locale("ru"),
        new Locale("lv"),
        new Locale("es", "CR"),
        new Locale("de")
    ));

    private static final ObjectProperty<Locale> locale = new SimpleObjectProperty<>(Locale.getDefault());

    static {
        locale.addListener(((observable, oldValue, newValue) -> Locale.setDefault(newValue)));
    }

    public static ObjectProperty<Locale> localeProperty()
    {
        return locale;
    }

    public static StringBinding $(String key, Object... args)
    {
        return Bindings.createStringBinding(() -> get(key, args), locale);
    }

    public static String get(final String key, final Object... args)
    {
        ResourceBundle bundle = ResourceBundle.getBundle("messages", getLocale());
        final String string;
        if (bundle.containsKey(key)) {
            string = bundle.getString(key);
        } else {
            return key;
        }
        String unicodeString;
        try {
            unicodeString = new String(string.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            unicodeString = string;
        }
        return MessageFormat.format(unicodeString, args);
    }

    public static Locale getLocale()
    {
        return locale.get();
    }

    public static void setLocale(Locale locale)
    {
        I18n.locale.set(locale);
    }

    public static Tooltip createTooltip(StringBinding binding)
    {
        final Tooltip tooltip = new Tooltip();
        tooltip.textProperty()
               .bind(binding);
        return tooltip;
    }

    public static Label createLabel(StringBinding binding)
    {
        Label label = new Label();
        label.textProperty()
             .bind(binding);
        return label;
    }
}
