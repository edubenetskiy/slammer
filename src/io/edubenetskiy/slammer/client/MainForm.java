package io.edubenetskiy.slammer.client;

import com.github.javafaker.Faker;
import io.edubenetskiy.slammer.model.Curiosity;
import io.edubenetskiy.slammer.model.Gender;
import io.edubenetskiy.slammer.model.Shorty;
import io.edubenetskiy.slammer.server.Server;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.SetChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static io.edubenetskiy.slammer.client.I18n.$;

public class MainForm extends Application
{
    private final ObjectProperty<DatabaseConnection> database;
    private final EditForm form;
    private final MenuBar menuBar;
    private final ToolBar toolBar;
    private final TreeTableView<Record> treeView;
    private final FilterForm filterForm;
    private final Scene filterScene;
    private Label statusBar;
    private StackPane statusBarStackPane;
    private Button btnDelete;
    private Button btnRemoveGreater;
    private FilteredTreeItem<Record> rootItem;
    private BorderPane root;
    private ProgressBar progressBar;

    public MainForm()
    {
        database = new SimpleObjectProperty<>(this, "database");
        form = new EditForm();
        filterForm = new FilterForm();
        filterScene = new Scene(filterForm, 480, 640);
        menuBar = createMenuBar();
        toolBar = createToolBar();
        statusBarStackPane = createStatusBarStackPane();
        treeView = createTreeTableView();
    }

    public static void main(String[] args)
    {
        launch(args);
    }

    private MenuBar createMenuBar()
    {
        MenuBar menuBar = new MenuBar();

        Menu menuFile = new Menu();
        menuFile.textProperty().bind($("mainWindow.menu.file"));

        MenuItem itmConnectServer = new MenuItem();
        itmConnectServer.textProperty().bind($("mainWindow.menu.file.connect"));
        itmConnectServer.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
        itmConnectServer.setOnAction(event -> {
            showConnectionDialog().ifPresent(serverAddress -> {
                new Thread(() -> {
                    Platform.runLater(() -> {
                        itmConnectServer.setDisable(true);
                        statusBar.setVisible(false);
                        progressBar.setVisible(true);
                    });
                    try {
                        final DatabaseConnection connection = new DatabaseConnection(serverAddress);
                        database.set(connection);
                    } catch (IOException | TimeoutException e) {
                        Platform.runLater(() -> {
                            final Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                            errorAlert.contentTextProperty()
                                      .bind($("connectionDialog.errorAlert.text"));
                            errorAlert.show();
                        });
                    } finally {
                        Platform.runLater(() -> {
                            itmConnectServer.setDisable(false);
                            progressBar.setVisible(false);
                            statusBar.setVisible(true);
                        });
                    }
                }).start();
            });
        });

        MenuItem itmDisconnect = new MenuItem();
        itmDisconnect.textProperty().bind($("mainWindow.menu.file.disconnect"));
        itmDisconnect.setOnAction(event -> disconnect());
        menuFile.getItems()
                .addAll(itmConnectServer,
                        itmDisconnect);

        Menu menuEdit = new Menu("Edit");
        menuEdit.textProperty().bind($("mainWindow.menu.edit"));

        MenuItem itmReset = new MenuItem();
        itmReset.textProperty().bind($("mainWindow.menu.edit.reset"));
        itmReset.disableProperty().bind(form.resettableProperty().not());
        itmReset.setOnAction(event -> form.reset());

        MenuItem itmSave = new MenuItem();
        itmSave.textProperty().bind($("mainWindow.menu.edit.save"));
        itmSave.disableProperty().bind(form.savableProperty().not());
        itmSave.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        itmSave.setOnAction(event -> form.save());

        MenuItem itmSaveIfMin = new MenuItem();
        itmSaveIfMin.textProperty().bind($("mainWindow.menu.edit.saveIfMin"));
        itmSaveIfMin.disableProperty().bind(form.savableProperty().not());
        itmSaveIfMin.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+S"));
        itmSaveIfMin.setOnAction(event -> form.saveIfMin());

        menuEdit.getItems().addAll(itmReset,
                                   itmSave,
                                   itmSaveIfMin);

        Menu menuHelp = new Menu();
        menuHelp.textProperty().bind($("mainWindow.menu.help"));
        final MenuItem aboutItem = new MenuItem(null, new ImageView(new Image(getClass().getResourceAsStream("images/help.png"))));
        aboutItem.textProperty().bind($("mainWindow.menu.help.about"));
        aboutItem.setOnAction(event -> showAboutWindow());
        final Menu languageItem = new Menu();
        languageItem.textProperty().bind($("mainWindow.menu.help.language"));
        languageItem.getItems().addAll(
            I18n.SUPPORTED_LOCALES
                .stream()
                .sorted(Comparator.comparing(Locale::getLanguage))
                .map(locale -> {
                    final MenuItem menuItem = new MenuItem(locale.getDisplayName());
                    final StringBinding binding = Bindings.createStringBinding(locale::getDisplayName, I18n.localeProperty());
                    menuItem.textProperty()
                            .bind(binding);
                    menuItem.setOnAction(e -> I18n.setLocale(locale));
                    return menuItem;
                })
                .collect(Collectors.toList()));
        menuHelp.getItems().add(aboutItem);
        menuHelp.getItems().add(languageItem);
        menuBar.getMenus().addAll(menuFile,
                                  menuEdit,
                                  menuHelp);
        return menuBar;
    }

    private ToolBar createToolBar()
    {
        ToolBar toolBar = new ToolBar();
        toolBar.setOrientation(Orientation.VERTICAL);
        toolBar.disableProperty().bind(database.isNull());

        Button btnCreate = new Button();
        btnCreate.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/create.png"))));
        btnCreate.setOnAction(event -> create());
        btnCreate.setTooltip(I18n.createTooltip($("mainWindow.toolBar.create.tooltip")));

        btnDelete = new Button();
        btnDelete.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/delete.png"))));
        btnDelete.setDisable(true);
        btnDelete.setOnAction(event -> delete());
        btnDelete.setTooltip(I18n.createTooltip($("mainWindow.toolBar.delete.tooltip")));

        Button btnScroll = new Button();
        btnScroll.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/reload.png"))));
        btnScroll.setOnAction(event -> scroll());
        btnScroll.setTooltip(I18n.createTooltip($("mainWindow.toolBar.refresh.tooltip")));

        Button btnClear = new Button();
        btnClear.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/delete-all.png"))));
        btnClear.setOnAction(event -> clear());
        btnClear.setTooltip(I18n.createTooltip($("mainWindow.toolBar.clear.tooltip")));

        btnRemoveGreater = new Button();
        btnRemoveGreater.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/delete-greater.png"))));
        btnDelete.setDisable(true);
        btnRemoveGreater.setOnAction(event -> removeGreater());
        btnRemoveGreater.setTooltip(I18n.createTooltip($("mainWindow.toolBar.deleteGreater.tooltip")));

        Button btnFilter = new Button();
        btnFilter.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("images/filter.png"))));
        btnFilter.setOnAction(event -> showFilterForm());
        btnFilter.setTooltip(I18n.createTooltip($("mainWindow.toolBar.filter.tooltip")));

        toolBar.getItems().addAll(btnCreate,
                                  btnDelete,
                                  btnRemoveGreater,
                                  btnClear,
                                  btnScroll,
                                  btnFilter);

        toolBar.getItems()
               .forEach(btn -> {
                   Button button = ((Button) btn);
                   button.setPrefSize(48, 48);
               });
        return toolBar;
    }

    private StackPane createStatusBarStackPane()
    {
        statusBar = new Label();
        statusBar.textProperty().bind($("mainWindow.statusBar.disconnected"));
        statusBar.setPadding(new Insets(8));
        database.addListener(
            (observable, oldValue, newValue) -> Platform.runLater(() -> {
                if (newValue == null) {
                    statusBar.textProperty()
                             .bind($("mainWindow.statusBar.disconnected"));
                } else {
                    statusBar.textProperty()
                             .bind($("mainWindow.statusBar.connected",
                                     database.get().getServerAddress()));
                    database.get().getRecords().addListener((SetChangeListener<Record>) change -> Platform.runLater(() -> {
                        if (change.wasAdded()) {
                            final TreeItem<Record> newItem = new TreeItem<>(change.getElementAdded());
                            rootItem.getAllChildren()
                                    .add(newItem);
                            // HACK: treeView.getSelectionModel().select(newItem);
                            treeView.getSelectionModel()
                                    .select(newItem);
                        } else {
                            final Object elementRemoved = change.getElementRemoved();
                            final TreeItem<Record> selectedItem = treeView.getSelectionModel()
                                                                          .getSelectedItem();
                            // HACK: This way the selected items are handled before sort
                            if (selectedItem != null && Objects.equals(selectedItem.getValue(), elementRemoved)) {
                                treeView.getSelectionModel()
                                        .clearSelection();
                            }
                            rootItem.getAllChildren()
                                    .removeIf(recordTreeItem -> Objects.equals(recordTreeItem.getValue(),
                                                                               elementRemoved));
                            if (Objects.equals(form.getEditedRecord(), elementRemoved)) {
                                form.setEditedRecord(null);
                            }
                        }
                        treeView.sort();
                    }));
                }
                form.setDatabaseConnection(newValue);
                scroll();
            })
        );

        progressBar = new ProgressBar(-1.0);
        progressBar.setPadding(new Insets(8));
        progressBar.setMaxWidth(Double.MAX_VALUE);
        progressBar.setVisible(false);

        statusBarStackPane = new StackPane(statusBar, progressBar);
        statusBarStackPane.setAlignment(Pos.CENTER_LEFT);
        return statusBarStackPane;
    }

    private TreeTableView<Record> createTreeTableView()
    {
        rootItem = new FilteredTreeItem<>(null);
        rootItem.setExpanded(true);
        rootItem.predicateProperty().bind(filterForm.predicateProperty());

        TreeTableColumn<Record, String> nameColumn = createNameColumn();
        TreeTableColumn<Record, Gender> genderColumn = createGenderColumn();
        TreeTableColumn<Record, Curiosity> curiosityColumn = createCuriosityColumn();
        TreeTableColumn<Record, ZonedDateTime> dateTimeColumn = createDateTimeColumn();
        TreeTableColumn<Record, Number> innocenceColumn = createInnocenceColumn();
        TreeTableColumn<Record, Boolean> registrationColumn = createRegistrationColumn();

        TreeTableView<Record> treeView = new TreeTableView<>(rootItem);
        treeView.getColumns().add(nameColumn);
        treeView.getColumns().add(genderColumn);
        treeView.getColumns().add(curiosityColumn);
        treeView.getColumns().add(dateTimeColumn);
        treeView.getColumns().add(innocenceColumn);
        treeView.getColumns().add(registrationColumn);
        treeView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    form.setEditedRecord(Optional.ofNullable(newValue)
                                                 .map(TreeItem::getValue)
                                                 .orElse(null));
                    btnDelete.setDisable(newValue == null);
                    btnRemoveGreater.setDisable(newValue == null);
                });
        treeView.setShowRoot(false);
        treeView.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case F5:
                case R:
                    scroll();
                    break;
                case DELETE:
                case SUBTRACT:
                    delete();
                    break;
                case INSERT:
                case ADD:
                case N:
                    create();
                    break;
            }
        });
        return treeView;
    }

    private Optional<InetSocketAddress> showConnectionDialog()
    {

        final Dialog<InetSocketAddress> dialog = new Dialog<>();
        dialog.titleProperty().bind($("connectionDialog.title"));
        dialog.headerTextProperty().bind($("connectionDialog.header"));

        final ButtonType connectButtonType = new ButtonType($("connectionDialog.connectButton").get(), ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes()
              .addAll(connectButtonType, ButtonType.CANCEL);

        final GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 20, 10, 10));

        final TextField hostnameField = new TextField("localhost");
        hostnameField.promptTextProperty().bind($("connectionDialog.hostname.prompt"));
        hostnameField.setMaxWidth(Double.MAX_VALUE);

        final Spinner<Integer> portField = new Spinner<>(1, 65535, Server.DEFAULT_PORT);
        portField.setEditable(true);
        portField.setMaxWidth(Double.MAX_VALUE);

        final ColumnConstraints column1 = new ColumnConstraints(Region.USE_COMPUTED_SIZE);
        final ColumnConstraints column2 = new ColumnConstraints();
        column2.setHgrow(Priority.ALWAYS);
        grid.getColumnConstraints().addAll(column1, column2);

        grid.add(I18n.createLabel($("connectionDialog.hostname.label")), 0, 0);
        grid.add(hostnameField, 1, 0);
        grid.add(I18n.createLabel($("connectionDialog.port.label")), 0, 1);
        grid.add(portField, 1, 1);

        final Node connectButton = dialog.getDialogPane()
                                         .lookupButton(connectButtonType);
        connectButton.disableProperty()
                     .bind(hostnameField.textProperty().isEmpty());

        dialog.getDialogPane()
              .setContent(grid);
        Platform.runLater(hostnameField::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton.equals(connectButtonType)) {
                return new InetSocketAddress(hostnameField.getText(), portField.getValue());
            }
            return null;
        });

        return dialog.showAndWait();
    }

    private void disconnect()
    {
        if (database.isNull().get()) return;
        database.get().close();
        database.set(null);
    }

    private void showAboutWindow()
    {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION, null);
        alert.titleProperty().bind($("mainWindow.menu.help.about"));
        alert.contentTextProperty().bind($("mainWindow.menu.help.about.text"));
        alert.setHeaderText("Slammer v0.2"); // TODO: Use I18n
        alert.showAndWait();
    }

    private void create()
    {
        if (database.get() == null) return;
        treeView.getSelectionModel().clearSelection();
        Record record = database.get().create();
        record.update(new Shorty(
            new Faker().name().fullName(),
            Gender.values()[new Random().nextInt(Gender.values().length)],
            ZonedDateTime.now(),
            Curiosity.values()[new Random().nextInt(Curiosity.values().length)],
            Math.random() * 100,
            Math.random() > 0.5));
        form.setEditedRecord(record);
    }

    private void delete()
    {
        if (database.isNull().get()) return;
        if (treeView.getSelectionModel().isEmpty()) return;
        Record record = treeView
            .getSelectionModel()
            .getSelectedItem()
            .getValue();
        database.get().delete(record);
    }

    // FIXME: If already connected, auto-scroll after connection does not deleteAll the view
    private void scroll()
    {
        treeView.getSelectionModel().clearSelection();
        // TODO: form.setEditedRecord(null);
        if (database.isNull().get()) {
            rootItem.getAllChildren().clear();
            final Label placeholder = new Label();
            placeholder.textProperty().bind($("mainWindow.treeView.noConnection"));
            treeView.setPlaceholder(placeholder);
        } else {
            database.get().scroll();
            /*
            rootItem.getAllChildren().deleteAll();
            rootItem.getAllChildren().addAll(
                database.get().getAllRecords().stream()
                    .map(TreeItem::new)
                    .collect(Collectors.toList()));
            */
            treeView.getSelectionModel().clearSelection();
            treeView.sort();
            final Label placeholder = new Label("No Records Found");
            placeholder.textProperty().bind($("mainWindow.treeView.noRecords"));
            treeView.setPlaceholder(placeholder);
        }
    }

    private void clear()
    {
        if (database.isNull().get()) return;
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                                      /* contentText: */ null,
                                      ButtonType.NO,
                                      ButtonType.YES);
        alert.contentTextProperty().bind($("mainWindow.toolBar.clear.alert"));

        final Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.isPresent() && Objects.equals(buttonType.get(), ButtonType.YES)) {
            form.setEditedRecord(null);
            database.get()
                    .clear();
        }
    }

    private void removeGreater()
    {
        if (database.isNull().get()) return;
        if (treeView.getSelectionModel().isEmpty()) return;
        Record record = treeView.getSelectionModel().getSelectedItem().getValue();
        database.get().removeGreater(record);
    }

    private void showFilterForm()
    {
        Stage stage = new Stage();
        stage.titleProperty().bind($("filterWindow.title"));
        stage.setScene(filterScene);
        stage.show();
    }

    private TreeTableColumn<Record, String> createNameColumn()
    {
        TreeTableColumn<Record, String> nameColumn = new TreeTableColumn<>();
        nameColumn.textProperty().bind($("model.name"));
        nameColumn.setPrefWidth(192);
        nameColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().nameProperty());
        nameColumn.setStyle("-fx-font-weight: bold;");
        return nameColumn;
    }

    private TreeTableColumn<Record, Gender> createGenderColumn()
    {
        TreeTableColumn<Record, Gender> genderColumn = new TreeTableColumn<>();
        genderColumn.textProperty()
                    .bind($("model.gender"));
        genderColumn.setPrefWidth(90);
        genderColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().genderProperty());
        genderColumn.setCellFactory(column -> new TreeTableCell<Record, Gender>()
        {
            @Override
            protected void updateItem(Gender item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    textProperty().unbind();
                    setText(null);
                } else {
                    textProperty().bind(item.getStringRepresentation());
                    setTextFill(item.getAssociatedColor());
                }
            }
        });
        return genderColumn;
    }

    private TreeTableColumn<Record, Curiosity> createCuriosityColumn()
    {
        TreeTableColumn<Record, Curiosity> curiosityColumn = new TreeTableColumn<>();
        curiosityColumn.textProperty().bind($("model.curiosity"));
        curiosityColumn.setPrefWidth(140);
        curiosityColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().curiosityProperty());
        curiosityColumn.setCellFactory(column -> new TreeTableCell<Record, Curiosity>()
        {
            @Override
            protected void updateItem(Curiosity item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    textProperty().unbind();
                    setText(null);
                } else {
                    // TODO: Apply I18n
                    textProperty().bind(Bindings.format("%d – %s", item.ordinal(), item.getStringRepresentation()));
                    setTextFill(item.getAssociatedColor());
                }
            }
        });
        return curiosityColumn;
    }

    private TreeTableColumn<Record, ZonedDateTime> createDateTimeColumn()
    {
        TreeTableColumn<Record, ZonedDateTime> tableColumn = new TreeTableColumn<>();
        tableColumn.textProperty()
                   .bind($("model.dateOfImprisonment"));
        tableColumn.setPrefWidth(250);
        tableColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().dateOfImprisonmentProperty());
        tableColumn.setCellFactory(column -> new TreeTableCell<Record, ZonedDateTime>()
        {
            @Override
            protected void updateItem(ZonedDateTime item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    textProperty().unbind();
                    setText(null);
                } else {
                    textProperty().bind(Bindings.createObjectBinding(() -> {
                        final DateTimeFormatter formatter = DateTimeFormatter
                            .ofLocalizedDateTime(FormatStyle.FULL)
                            .withLocale(I18n.getLocale());
                        return item.format(formatter);
                    }, I18n.localeProperty()));
                }
            }
        });
        return tableColumn;
    }

    private TreeTableColumn<Record, Number> createInnocenceColumn()
    {
        TreeTableColumn<Record, Number> innocenceColumn = new TreeTableColumn<>();
        innocenceColumn.textProperty()
                       .bind($("model.innocence"));
        innocenceColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().innocenceProperty());
        innocenceColumn.setCellFactory(column -> new TreeTableCell<Record, Number>()
        {
            @Override
            protected void updateItem(Number item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(String.format("%.1f", item));
                }
            }
        });
        innocenceColumn.setStyle("-fx-alignment: center-right;");
        return innocenceColumn;
    }

    private TreeTableColumn<Record, Boolean> createRegistrationColumn()
    {
        TreeTableColumn<Record, Boolean> registrationColumn = new TreeTableColumn<>();
        registrationColumn.textProperty()
                          .bind($("model.registration"));
        registrationColumn.setPrefWidth(96);
        registrationColumn.setCellValueFactory(column -> column.getValue().getValue().getShorty().registeredProperty());
        registrationColumn.setCellFactory(column -> new TreeTableCell<Record, Boolean>()
        {
            @Override
            protected void updateItem(Boolean item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);
                    setStyle("");
                } else {
                    setText(item ? "✓" : "✗");
                    setStyle("-fx-alignment: center");
                }
            }
        });
        return registrationColumn;
    }

    @Override
    public void start(Stage primaryStage)
    throws Exception
    {
        root = new BorderPane();
        root.setTop(menuBar);
        root.setLeft(toolBar);
        root.setBottom(statusBarStackPane);
        root.setCenter(new SplitPane(treeView, form));
        scroll();

        primaryStage.titleProperty().bind($("mainWindow.title"));
        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.setOnCloseRequest(event -> disconnect());
        primaryStage.show();
    }
}
