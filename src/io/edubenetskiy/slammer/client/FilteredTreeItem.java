package io.edubenetskiy.slammer.client;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TreeItem;

import java.util.function.Predicate;

public class FilteredTreeItem<T> extends TreeItem<T>
{
    private final ObservableList<TreeItem<T>> allChildren;
    private final FilteredList<TreeItem<T>> filteredChildren;
    private final ObjectProperty<Predicate<T>> predicate;

    public FilteredTreeItem(T value)
    {

        super(value);

        allChildren = FXCollections.observableArrayList();
        filteredChildren = new FilteredList<>(allChildren);
        predicate = new SimpleObjectProperty<>();

        filteredChildren.predicateProperty().bind(
            Bindings.createObjectBinding(() -> child -> {
                if (child instanceof FilteredTreeItem) {
                    ((FilteredTreeItem<T>) child).predicateProperty()
                                                 .set(predicate.get());
                }
                return predicate.get() == null ||
                       child.getChildren()
                            .size() > 0 ||
                       predicate.get()
                                .test(child.getValue());
            }, predicate));

        filteredChildren.addListener((ListChangeListener<TreeItem<T>>) change -> {
            while (change.next()) {
                getChildren().removeAll(change.getRemoved());
                getChildren().addAll(change.getAddedSubList());
            }
        });
    }

    public ObjectProperty<Predicate<T>> predicateProperty()
    {
        return predicate;
    }

    public ObservableList<TreeItem<T>> getAllChildren()
    {
        return allChildren;
    }
}
