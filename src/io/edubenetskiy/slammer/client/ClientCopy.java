package io.edubenetskiy.slammer.client;

import io.edubenetskiy.slammer.model.Shorty;
import javafx.beans.property.ReadOnlySetWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public class ClientCopy
{
    private final ObservableSet<Record> records;
    private final ReadOnlySetWrapper<Record> readOnlyCollection;

    public ClientCopy()
    {
        this.records = FXCollections.observableSet();
        this.readOnlyCollection = new ReadOnlySetWrapper<>(records);
    }

    public void scroll(Collection<Record> scroll)
    {
        records.clear();
        records.addAll(scroll);
    }

    public void clear()
    {
        records.clear();
    }

    public void insert(int id, Shorty shorty)
    {
        records.add(new Record(id, shorty));
    }

    public void delete(int id)
    {
        records.removeIf(record -> record.getId() == id);
    }

    public void removeGreater(int id)
    {
        final Optional<Record> pivotRecord =
            records.stream()
                   .filter(record -> record.getId().equals(id))
                   .findFirst();
        if (pivotRecord.isPresent()) {
            final Shorty pivot = pivotRecord.get().getShorty();
            records.removeIf(record -> record.getShorty().isGreaterThan(pivot));
        } else {
            // Record not found
            // TODO: Process synchronization error
            System.out.println("ERR: Unprocessed synchronization error");
        }
    }

    public ReadOnlySetWrapper<Record> getReadOnlyCollection()
    {
        return readOnlyCollection;
    }

    public void update(Integer id, Shorty shorty)
    {
        if (id == null) return; // REVIEW: Probably raise an exception
        records.stream()
               .filter(record -> Objects.equals(record.getId(), id))
               .forEach(record -> record.update(shorty));
    }
}
