package io.edubenetskiy.slammer.client;

import com.sun.javafx.collections.ImmutableObservableList;
import io.edubenetskiy.slammer.model.Gender;
import io.edubenetskiy.slammer.model.Shorty;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;

import java.util.Arrays;
import java.util.function.Predicate;

import static io.edubenetskiy.slammer.client.I18n.$;

public class FilterForm extends GridPane
{
    private final TextField nameField;
    private final ComboBox<Gender> genderComboBox;
    private final Slider innocenceSlider;
    private final CuriosityToggleGroup curiosityToggleGroup;
    private final ToggleButton registrationToggleButton;

    private final ObjectBinding<Predicate<Record>> predicate;

    FilterForm()
    {
        super();

        Label changedLabel = new Label();
        changedLabel.setFont(Font.font(16));
        changedLabel.setTextAlignment(TextAlignment.RIGHT);

        Label stateLabel = I18n.createLabel($("filterWindow.title"));
        stateLabel.setFont(Font.font(16));

        nameField = new TextField();
        nameField.setMaxWidth(Double.MAX_VALUE);
        final CheckBox nameCheckBox = new CheckBox();

        genderComboBox = new ComboBox<>(new ImmutableObservableList<>(Gender.values()));
        genderComboBox.setMaxWidth(Double.MAX_VALUE);
        genderComboBox.setCellFactory(param -> new ListCell<Gender>()
        {
            @Override
            protected void updateItem(Gender item, boolean empty)
            {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    textProperty().unbind();
                    setText(null);
                } else {
                    textProperty().bind(item.getStringRepresentation());
                    setTextFill(item.getAssociatedColor());
                }
            }
        });
        I18n.localeProperty()
            .addListener((observable, oldValue, newValue) -> {
                final Gender value = genderComboBox.getValue();
                genderComboBox.setValue(null);
                genderComboBox.setValue(value);
            });
        genderComboBox.setConverter(new StringConverter<Gender>()
        {
            @Override
            public String toString(Gender object)
            {
                return object.getStringRepresentation()
                             .get();
            }

            @Override
            public Gender fromString(String string)
            {
                return Arrays.stream(Gender.values())
                             .filter(g -> g.getStringRepresentation()
                                           .get()
                                           .equals(string))
                             .findFirst()
                             .orElse(null);
            }
        });
        final CheckBox genderCheckBox = new CheckBox();

        Label innocenceLabel = new Label();
        innocenceSlider = new Slider(0, 100, 50);
        innocenceLabel.setMinWidth(42);
        innocenceLabel.setAlignment(Pos.CENTER_RIGHT);
        innocenceLabel.textProperty().bind(innocenceSlider.valueProperty().asString("%.1f"));
        innocenceSlider.adjustValue(0);
        innocenceSlider.setShowTickMarks(true);
        innocenceSlider.setMaxWidth(Double.MAX_VALUE);
        final CheckBox innocenceCheckBox = new CheckBox();

        final HBox innocenceBox = new HBox(innocenceSlider, innocenceLabel);
        innocenceBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(innocenceSlider, Priority.ALWAYS);

        registrationToggleButton = new ToggleButton("✓");
        final CheckBox registrationCheckBox = new CheckBox();

        curiosityToggleGroup = new CuriosityToggleGroup();
        final CheckBox curiosityCheckBox = new CheckBox();

        add(stateLabel, 0, 0, 2, 1);

        add(I18n.createLabel($("model.name")), 0, 1);
        add(nameField, 1, 1);
        add(nameCheckBox, 2, 1);

        add(I18n.createLabel($("model.gender")), 0, 2);
        add(genderComboBox, 1, 2);
        add(genderCheckBox, 2, 2);

        add(I18n.createLabel($("model.innocence")), 0, 3);
        add(innocenceBox, 1, 3);
        add(innocenceCheckBox, 2, 3);

        add(I18n.createLabel($("model.registration")), 0, 4);
        add(registrationToggleButton, 1, 4);
        add(registrationCheckBox, 2, 4);

        add(I18n.createLabel($("model.curiosity")), 0, 5);
        add(curiosityToggleGroup, 1, 5);
        add(curiosityCheckBox, 2, 5);

        final ColumnConstraints firstColumnConstraints = new ColumnConstraints(Region.USE_COMPUTED_SIZE);
        final ColumnConstraints secondColumnConstraints = new ColumnConstraints();
        secondColumnConstraints.setHgrow(Priority.ALWAYS);
        getColumnConstraints().add(firstColumnConstraints);
        getColumnConstraints().add(secondColumnConstraints);

        setPadding(new Insets(8));
        setHgap(8);
        setVgap(8);

        setMinWidth(256);
        setMaxWidth(512);

        final double EPSILON = 0.1D;
        predicate = Bindings.createObjectBinding(
            () -> (Record record) -> {
                Shorty shorty = record.getShorty();
                return (!nameCheckBox.isSelected() || shorty.getName().contains(nameField.getText())) &&
                       (!genderCheckBox.isSelected() || genderComboBox.getValue().equals(shorty.getGender())) &&
                       (!curiosityCheckBox.isSelected() || curiosityToggleGroup.getValue().equals(shorty.getCuriosity())) &&
                       (!innocenceCheckBox.isSelected() || Math.abs(innocenceSlider.getValue() - shorty.getInnocence()) < EPSILON) &&
                       (!registrationCheckBox.isSelected() || registrationToggleButton.isSelected() == shorty.isRegistered());
            },
            nameCheckBox.selectedProperty(), nameField.textProperty(),
            genderCheckBox.selectedProperty(), genderComboBox.valueProperty(),
            curiosityCheckBox.selectedProperty(), curiosityToggleGroup.valueProperty(),
            innocenceCheckBox.selectedProperty(), innocenceSlider.valueProperty(),
            registrationCheckBox.selectedProperty(), registrationToggleButton.selectedProperty());
    }

    public ObjectBinding<Predicate<Record>> predicateProperty()
    {
        return predicate;
    }
}
