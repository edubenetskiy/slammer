package io.edubenetskiy.slammer.client;

import io.edubenetskiy.slammer.model.Curiosity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

public class CuriosityToggleGroup extends VBox
{
    private final ToggleGroup toggleGroup;
    private final ObjectProperty<Curiosity> value;

    CuriosityToggleGroup()
    {
        super();
        value = new SimpleObjectProperty<>(this, "value");
        toggleGroup = new ToggleGroup();

        for (Curiosity curiosity : Curiosity.values()) {
            RadioButton radioButton = new RadioButton();
            radioButton.textProperty().bind(curiosity.getStringRepresentation());
            radioButton.setUserData(curiosity);
            toggleGroup.getToggles().add(radioButton);
            getChildren().add(radioButton);
        }
        value.addListener((observable, oldValue, newValue) -> {
            toggleGroup.getToggles()
                       .filtered(toggle -> toggle.getUserData().equals(newValue))
                       .forEach(toggle -> toggle.setSelected(true));
        });
        toggleGroup.selectedToggleProperty()
                   .addListener((observable, oldValue, newValue) -> {
                       if (newValue == null) {
                           setValue(null);
                       } else {
                           value.setValue((Curiosity) (newValue.getUserData()));
                       }
                   });
        value.set(Curiosity.TOLERABLE);
        setSpacing(4);
    }

    public ObjectProperty<Curiosity> valueProperty()
    {
        return value;
    }

    public Curiosity getValue()
    {
        return value.get();
    }

    public void setValue(Curiosity value)
    {
        this.value.set(value);
    }
}
