package io.edubenetskiy.slammer.client;

import io.edubenetskiy.slammer.model.Shorty;
import io.edubenetskiy.slammer.protocol.requests.*;
import io.edubenetskiy.slammer.protocol.responses.Response;
import javafx.beans.property.ReadOnlySetWrapper;

import java.io.*;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.DatagramChannel;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.*;

public class DatabaseConnection
{
    private static final int BUFFER_CAPACITY = 65535;
    private static final int HANDSHAKE_ATTEMPTS = 10;
    private static final int HANDSHAKE_TIMEOUT = 500;

    private final Thread receiptThread;
    private final ClientCopy clientCopy;
    private final SocketAddress serverAddress;
    private final DatagramChannel channel;
    private final Random random;
    private final HashMap<Long, Request> requestQueue;

    /**
     * Creates a DatabaseConnection connected to the given server address.
     *
     * @param serverAddress the SocketAddress to connect to
     *
     * @throws IOException if there was an error on binding to the local host
     */
    public DatabaseConnection(SocketAddress serverAddress)
    throws IOException, TimeoutException
    {

        this.serverAddress = serverAddress;
        this.channel = DatagramChannel.open();
        this.channel.bind(null);
        System.out.printf("-i- (%s) Bound to %s\n", LocalDateTime.now(), channel.getLocalAddress());
        System.out.printf("-i- (%s) Connected to %s\n", LocalDateTime.now(), serverAddress);

        this.requestQueue = new HashMap<>();
        this.random = new Random();
        this.clientCopy = new ClientCopy();

        handshake();

        receiptThread = new Thread(() -> {
            System.out.printf("-i- (%s) Listening\n", LocalDateTime.now());
            while (channel.isOpen()) {
                Response response = receive();
                if (response == null) continue;
                response.execute(clientCopy);
                // TODO: Process errors
            }
        });
        receiptThread.start();
    }

    private void handshake()
    throws TimeoutException
    {
        final FutureTask<Response> task = new FutureTask<>(this::receive);
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        Response handshake = null;
        executor.execute(task);
        for (int i = 0; i < HANDSHAKE_ATTEMPTS; i++) {
            send(new NoopRequest());
            try {
                handshake = task.get(HANDSHAKE_TIMEOUT, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
            }
            if (task.isDone() && handshake != null) {
                handshake.execute(clientCopy);
                break;
            }
        }
        task.cancel(true);
        executor.shutdownNow();
        if (handshake == null) {
            throw new TimeoutException("connection timeout");
        }
    }

    private Response receive()
    {
        Response response = null;
        final ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_CAPACITY);
        try {
            channel.receive(byteBuffer);
            byteBuffer.flip();
            try (ByteArrayInputStream byteStream = new ByteArrayInputStream(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.remaining());
                 ObjectInputStream objectStream = new ObjectInputStream(byteStream)) {
                response = (Response) objectStream.readObject();
                System.out.printf("<-- (%s) %s\n", LocalDateTime.now(), response);
            } catch (ClassCastException | ClassNotFoundException e) {
                // TODO: Process invalid response somehow
                e.printStackTrace();
            }
        } catch (ClosedByInterruptException e) {
            // It's all fine, ignore.
        } catch (IOException e) {
            // Ignore. // FIXME
            e.printStackTrace();
        }
        return response;
    }

    private void send(Request request)
    {
        System.out.printf("--> (%s) %s\n", LocalDateTime.now(), request);
        try {
            ByteBuffer bytes = ByteBuffer.allocate(BUFFER_CAPACITY);
            bytes.clear();
            try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream()) {
                try (ObjectOutputStream objectStream = new ObjectOutputStream(byteStream)) {
                    final long id = random.nextLong();
                    objectStream.writeLong(id);
                    objectStream.writeObject(request);
                    requestQueue.put(id, request);
                }
                bytes.put(byteStream.toByteArray());
                bytes.flip();
                channel.send(bytes, serverAddress);
            }
        } catch (IOException e) {
            // TODO: Process IOException
            e.printStackTrace();
        }
    }

    public void delete(Record record)
    {
        send(new DeleteRequest(record.getId()));
    }

    public Record create()
    {
        return new Record();
    }

    public void insert(Shorty shorty)
    {
        send(new InsertRequest(shorty));
    }

    public boolean isMin(Record record)
    {
        Optional<Record> min = getRecords().stream().min(Comparator.comparing(Record::getShorty));
        return !min.isPresent() ||
               record.equals(min.get()) ||
               record.getShorty().isLessThan(min.get().getShorty());
    }

    public ReadOnlySetWrapper<Record> getRecords()
    {
        return this.clientCopy.getReadOnlyCollection();
    }

    public void removeGreater(Record record)
    {
        send(new RemoveGreaterRequest(record.getId()));
    }

    public void clear()
    {
        send(new ClearRequest());
    }

    Optional<Record> findById(int id)
    {
        return getRecords().stream()
                           .filter(record -> record.getId() == id)
                           .findFirst();
    }

    public void close()
    {
        System.out.printf("-i- (%s) Receipt thread stopped\n", LocalDateTime.now());
        try {
            receiptThread.interrupt();
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void update(Integer id, Shorty shorty)
    {
        send(new UpdateRequest(id, shorty));
    }

    public void scroll()
    {
        send(new ScrollRequest());
    }

    public Object getServerAddress()
    {
        return serverAddress;
    }
}
