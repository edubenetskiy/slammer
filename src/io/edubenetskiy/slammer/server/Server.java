package io.edubenetskiy.slammer.server;

import io.edubenetskiy.slammer.protocol.requests.Request;
import io.edubenetskiy.slammer.protocol.responses.FailureResponse;
import io.edubenetskiy.slammer.protocol.responses.Response;

import java.io.*;
import java.net.*;
import java.time.LocalDateTime;

public class Server
{
    public static final int DEFAULT_PORT = 26723;
    private static final int BUFFER_CAPACITY = 8192;

    private final Database database;
    private DatagramSocket socket;

    public Server()
    {
        database = new Database();
    }

    public static void main(String[] args)
    {
        new Server().run();
    }

    public void run()
    {
        bind();
        while (true) {
            try {
                receive();
            } catch (IOException e) {
                // TODO: Process IOException-terminated receipt
                e.printStackTrace();
            }
        }
    }

    private void bind()
    {
        try {
            SocketAddress address = new InetSocketAddress(DEFAULT_PORT);
            socket = new DatagramSocket(address);
            System.out.printf("-i- (%s) Bound to %s\n",
                              LocalDateTime.now(),
                              socket.getLocalSocketAddress());
        } catch (SocketException e) {
            // Unreachable
        }
    }

    private void receive()
    throws IOException
    {
        final byte[] bytes = new byte[BUFFER_CAPACITY];
        final DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
        socket.receive(packet);
        new Thread(() -> {
            // Identify client
            SocketAddress client = packet.getSocketAddress();
            // TODO: Check if the client is up-to-date
            try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes, packet.getOffset(), packet.getLength())) {
                try (ObjectInputStream objectStream = new ObjectInputStream(byteStream)) {
                    Long id = objectStream.readLong();
                    Request request = (Request) objectStream.readObject();
                    Response response = request.execute(database);
                    System.out.printf("<-- (%s) %s\n", LocalDateTime.now(), request);
                    send(client, response);
                } catch (ClassNotFoundException e) {
                    // TODO: Notify of wrong request
                    send(client, new FailureResponse(null));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void send(SocketAddress destination, Response response)
    {
        System.out.printf("--> (%s) %s\n", LocalDateTime.now(), response);
        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objectStream = new ObjectOutputStream(byteStream)) {
                objectStream.writeObject(response);
            }
            byte[] bytes = byteStream.toByteArray();
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, destination);
            socket.send(packet);
        } catch (IOException e) {
            // TODO: Process IOException
            e.printStackTrace();
        }
    }
}
