package io.edubenetskiy.slammer.server;

import io.edubenetskiy.justorm.dao.DataAccessObject;
import io.edubenetskiy.justorm.dao.DataAccessObjectManager;
import io.edubenetskiy.slammer.client.Record;
import io.edubenetskiy.slammer.model.Shorty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Database
{
    private final DataAccessObject<Shorty, Integer> dao;
    private int version;
    private Connection connection;

    Database()
    {
        final String url = "jdbc:postgresql://localhost:5432/postgres";
        final String username = "postgres";
        final String password = "postgres";

        try {
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            // TODO: Handle SQLException on connection
            e.printStackTrace();
        }
        dao = new DataAccessObjectManager(connection).getDao(Shorty.class);
        version = 1;
    }

    public List<Record> getAllRecords()
    {
        try {
            return dao.fetchAll().stream()
                      .map((Shorty shorty) -> new Record(shorty.getId(), shorty))
                      .collect(Collectors.toList());
        } catch (SQLException e) {
            // TODO: Process SQLException in method ‘getAllRecords’
            e.printStackTrace();
            return null;
        }
    }

    public void deleteAll()
    {
        try {
            dao.deleteAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        version++;
    }

    // TODO: Implement internal SQL query builder
    public void update(Integer id, Shorty updated)
    {
        try {
            String sql = "UPDATE public.shorties SET name = ?, gender = ?, curiosity = ?, innocence = ?, registered = ? WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, updated.getName());
            statement.setString(2, updated.getGender().toString());
            statement.setString(3, updated.getCuriosity().toString());
            statement.setDouble(4, updated.getInnocence());
            statement.setBoolean(5, updated.isRegistered());
            statement.setInt(6, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            // TODO: Process SQLException on execution of method ‘update’
            e.printStackTrace();
        }
        // TODO: Implement version control
        version++;
    }

    public void removeGreater(int id)
    {
        try {
            final String sql =
                "DELETE " +
                "FROM public.shorties " +
                "WHERE innocence > (" +
                "    SELECT innocence " +
                "    FROM shorties " +
                "    WHERE id = ?" +
                ")";
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            // TODO: Process SQLException on execution of method ‘removeGreater’
            e.printStackTrace();
        }
        version++;
    }

    public Optional<Integer> create(Shorty shorty)
    {
        try {
            dao.create(shorty);
            version++;
            return Optional.of(shorty.getId());
        } catch (SQLException e) {
            e.printStackTrace(); // TODO
            return Optional.empty();
        }
    }

    public boolean delete(int id)
    {
        try {
            version++;
            return dao.deleteByKey(id);
        } catch (SQLException e) {
            // TODO: Process SQLException on execution of method ‘delete’
            e.printStackTrace();
            return false;
        }
    }

    public int getVersion()
    {
        return version;
    }
}
